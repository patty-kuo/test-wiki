 ## OPL1000系列  
### OPL1000
    - Wi-Fi 11b 至11Mbps
    - BT 5.0 le + 2Mbps 数据速率 
    - 出色的射频性能 
    - 超低功耗 
    - 双 ARM 核心 M0/m3 
    - 广泛的外围设备 
    - 集成 PMU 
    - 完整的硬件安全加密引擎 
    - 直流模拟输入   
![OPL1000_pin](https://bitbucket.org/patty-kuo/test-wiki/src/master/%E7%94%A2%E5%93%81/OPL1000_pin.PNG) 
  
📓 [[Doc] OPL1000-DataSheet-NonNDA](https://github.com/Opulinks-Tech/OPL1000-HDK/blob/master/OPL1000-DS-NonNDA.pdf)  
 