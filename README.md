[![home_button](https://github.com/patty-kuo/WIKI_test/blob/master/home_button2.png)](https://github.com/patty-kuo/WIKI_test/wiki)  [![product_button](https://github.com/patty-kuo/WIKI_test/blob/master/product_button2.PNG)](https://github.com/patty-kuo/WIKI_test/wiki/product)  [![SDK_button](https://github.com/patty-kuo/WIKI_test/blob/master/SDK2.PNG)](https://github.com/patty-kuo/WIKI_test/wiki/SDK)  [![HDK_button](https://github.com/patty-kuo/WIKI_test/blob/master/HDK.PNG)](https://github.com/patty-kuo/WIKI_test/wiki/HDK)  [![doc_button](https://github.com/patty-kuo/WIKI_test/blob/master/doccument_button2.PNG)](https://github.com/patty-kuo/WIKI_test/wiki/Documents)  [![demo&ref_button](https://github.com/patty-kuo/WIKI_test/blob/master/ref_button.png)](https://github.com/patty-kuo/WIKI_test/wiki/reference_demo)  [![tool_button](https://github.com/patty-kuo/WIKI_test/blob/master/tool_button2.PNG)](https://github.com/patty-kuo/WIKI_test/wiki/tools) 



# 旺凌科技

深圳旺凌科技有限公司 (Opulinks Technology) 成立于2016年，是一家专业从事物联网WIFI/BLE双模芯片的研发、设计并提供整套解决方案的高新技术企业。旺凌科技设计的OPL1000芯片是一种嵌入式多标准 2.4 ghz 无线连接解决方案。它将 Wi-Fi 和蓝牙智能集成到一个单一的 SoC 中, 使物联网系统能够更轻松地进行互连设计并连接到云,并具有更高的灵活性。专为需要低功耗的新兴物联网产品应用而设计, 从零开始。它提供了业界最低的功耗、最高的性能、轻松的连接、可扩展的网络和最低的系统成本。  

:computer:公司网页[【请点此】](https://opulinks.com/zh/)　　

# 发展历程

* 2015.04 - 在美国南加州Irvan组织射频和通信系统研发团队
* 2015.07 - 在台湾注册成立 NetlinkC
* 2016.12 - 注册成立 Opulinks,并在上海建立分公司
* 2017.08 - 在深圳注册成立Opulinks深圳分公司

# 主要产品
![OPL1000](https://github.com/patty-kuo/WIKI_test/blob/master/OPL1000_S.PNG)  
:link:OPL1000 WIFI/BLE双模低功耗芯片[【网页介紹】](https://opulinks.com/zh/products/) | [【产品设计介紹】](https://github.com/patty-kuo/WIKI_test/wiki/product)   